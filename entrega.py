import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import decimal
import plotly.offline as pyo
import plotly.graph_objects as go
from sklearn.preprocessing import StandardScaler

df = pd.read_csv('data.csv', sep=';')
df = df.drop(df.columns[0], axis=1)
# print(df.head())
# print(df.head())


def camb_error(data, col, min, max):
    for i in data[col]:
        if i < min:
            data = data.replace(i, 0)
        elif i > max:
            data = data.replace(i, 0)


# Al Parecer la edad esta en dias, la altura en [cm], el peso en [kg] y otros valores en buliano. Hay dos headers que aun no se que son, ap_hi y ap_lo
# print(f'Shape :{df.shape}')
# print(df.info())
# l
# print(df.isnull().sum())
df = df.dropna()

# print(df.isnull().sum())


# dejar la variable en altura en metros
# agregar columna de imc que relaciona peso y altura
df['age'] = df['age']/365
df['age'] = pd.Series([round(val, 0)]for val in df['age'])
df['height'] = df['height']/100
df_weight_2 = df['weight']**2
df['imc'] = (df['height']/df_weight_2)*10000
imc_ascendente = df.sort_values(by='imc', ascending=False)
# print(imc_ascendente.head())

# for usado para recorrer los valores y ver que no hayan incorrectos (todos los valores binarios fueron analizados)


# df['ap_hi']=pd.qcut(df['ap_hi'], q=3,labels=False, retbins=False, precision=3, duplicates='raise')
# df['ap_hi']=df['ap_hi'].reset_index(drop = True)


# discretizacion
df['imc'] = pd.qcut(df['imc'], q=3, labels=False,
                    retbins=False, precision=3, duplicates='raise')
df['imc'] = df['imc'].reset_index(drop=True)
# df['ap_lo']=pd.qcut(df['ap_lo'], q=3,labels=False, retbins=False, precision=3, duplicates='drop')
# df['ap_lo']=df['ap_lo'].reset_index(drop = True)

print(df['ap_hi'].max())
print(df['ap_hi'].min())
# Si hay valores menores a 0 y mayores a 110 son erroneos, estarian muertos
print(df['ap_lo'].max())
print(df['ap_lo'].min())
# si hay valores mayores a 180 y menores a 0 son erroneos, estarian muertos

#camb_error(df, 'ap_hi', 0, 110)
# for i in df['ap_hi']:
#   if i < 0:
#       df = df.replace(i, 0)
#   elif i > 110:
#       df = df.replace(i, 0)

#x = 0
# print(x)
# df['ap_hi']:
# == 0:
#x += 1
print("cant de datos erroneos, presion diastólica")

# for j in df['ap_lo']:
#    if j < 0:
#        df = df.replace(j, 0)
#    elif j > 180:
#        df = df.replace(j, 0)
#
#y = 0
# for j in df['ap_lo']:
#    if j == 0:
#        y += 1
#print("cant de datos erroneos, presion sistólica", y)

print(df['ap_hi'].max())
print(df['ap_hi'].min())
print(df['ap_lo'].max())
print(df['ap_lo'].min())
presion = []
for i in df['ap_hi']:
    for j in df['ap_lo']:
        if i == 1 and j == 1:
            presion.append('normal')
        elif(i == 2 | j == 2):
            presion.append('alta')
        else:
            presion.append('baja')
        break
print(presion)

df['tipo_presion'] = presion
print(df.head())

# 111 son la cantidad de muestras diferentes que hay
plt.hist(presion, bins=111, edgecolor='black')
plt.title("presion arterial Alta")
plt.axis([60, 225, 0, 30000])
plt.xlabel("presiones medidas")
plt.ylabel("Muestras")
plt.show()


# print(df['tipo_presion'])

# for i in df['ap_lo']:
# if(x<i):
#                x=i
#       else:
#              x=x
# print(x)

# relacion presencia de ec y otras caract por sexo femenino
df_mujeres = df[df['gender'] == 1]
print(df_mujeres.head())
# filtros
Fumadoras = df_mujeres['smoke'] == 1
Bebedoras = df_mujeres['alco'] == 1
Glucosa_alta = df_mujeres['gluc'] > 1
colesterol_alto = df_mujeres['cholesterol'] > 1
